package com.karkon.ticket_booking.controller;

import com.karkon.ticket_booking.dto.GenreDTO;
import com.karkon.ticket_booking.repository.GenreRepository;
import com.karkon.ticket_booking.service.GenreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/genre")
@Api(tags = "Genre Controller")
public class GenreController {

    private final GenreRepository genreRepository; //do testow
    private final GenreService genreService;

    @GetMapping("/test")
    public ResponseEntity<String> getGenres() {
        return ResponseEntity.ok(genreRepository.findAll().toString());
    }

    @GetMapping("/details")
    @ApiOperation(value = "Genre Details", notes = "Metoda umozliwiajaca pobranie informacji szczegolowych o gatunku filmowym.")
    public ResponseEntity<List<GenreDTO>> getGenreDetails(@RequestParam final String genreId) {
        return ResponseEntity.ok(genreService.getGenreDetailsByGenreId(genreId));
    }
}
