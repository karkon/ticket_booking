package com.karkon.ticket_booking.controller;

import com.karkon.ticket_booking.dto.ErrorDTO;
import com.karkon.ticket_booking.service.exception.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(NoRecordException.class)
    public ResponseEntity<ErrorDTO> NoRecordExceptionHandler(NoRecordException noRecordException) {
        return ResponseEntity.badRequest().body(new ErrorDTO(noRecordException.getMessage()));
    }

    @ExceptionHandler(InvalidSeatException.class)
    public ResponseEntity<ErrorDTO> InvalidSeatExceptionHandler(InvalidSeatException invalidSeatException) {
        return ResponseEntity.badRequest().body(new ErrorDTO(invalidSeatException.getMessage()));
    }

    @ExceptionHandler(InvalidTermException.class)
    public ResponseEntity<ErrorDTO> InvalidTermExceptionHandler(InvalidTermException invalidTermException) {
        return ResponseEntity.badRequest().body(new ErrorDTO(invalidTermException.getMessage()));
    }

    @ExceptionHandler(TakenSeatException.class)
    public ResponseEntity<ErrorDTO> TakenSeatExceptionHandler(TakenSeatException takenSeatException) {
        return ResponseEntity.badRequest().body(new ErrorDTO(takenSeatException.getMessage()));
    }

    @ExceptionHandler(CreateUserException.class)
    public ResponseEntity<ErrorDTO> CreateUserExceptionHandler(CreateUserException createUserException) {
        return ResponseEntity.badRequest().body(new ErrorDTO(createUserException.getMessage()));
    }

    @ExceptionHandler(PatternException.class)
    public ResponseEntity<ErrorDTO> PatternExceptionHandler(PatternException patternException) {
        return ResponseEntity.badRequest().body(new ErrorDTO(patternException.getMessage()));
    }

    @ExceptionHandler(LoginUserException.class)
    public ResponseEntity<ErrorDTO> LoginUserExceptionHandler(LoginUserException loginUserException) {
        return ResponseEntity.badRequest().body(new ErrorDTO(loginUserException.getMessage()));
    }
}
