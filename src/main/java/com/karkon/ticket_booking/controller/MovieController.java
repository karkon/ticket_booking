package com.karkon.ticket_booking.controller;

import com.karkon.ticket_booking.dto.MovieDTO;
import com.karkon.ticket_booking.service.MovieService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/movie")
@Api(tags = "Movie Controller")
public class MovieController {


    private final MovieService movieService;

    @GetMapping("/details")
    @ApiOperation(value = "Movie Details", notes = "Metoda umozliwiajaca pobranie informacji szczegolowych o filmie.")
    public ResponseEntity<MovieDTO> getMovieDetails(@RequestParam final Integer movieId) {
        return ResponseEntity.ok(movieService.getDetailsById(movieId));
    }


}
