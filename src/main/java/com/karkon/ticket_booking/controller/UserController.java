package com.karkon.ticket_booking.controller;

import com.karkon.ticket_booking.dto.LoginDTO;
import com.karkon.ticket_booking.dto.LoginResultDTO;
import com.karkon.ticket_booking.dto.UserDTO;
import com.karkon.ticket_booking.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
@Api(tags = "User Controller")
public class UserController {

    private final UserService userService;

    @PostMapping("/create")
    @ApiOperation(value = "Create User", notes = "Metoda umozliwiajaca utworzenie konta uzytkownika.")
    public ResponseEntity<String> createUser(@RequestBody UserDTO userDTO) {
        return ResponseEntity.ok(userService.createUser(userDTO));
    }

    @PostMapping("/login")
    @ApiOperation(value = "Login User", notes = "Metoda umozliwiajaca zalogowanie uzytkownika")
    public ResponseEntity<LoginResultDTO> loginUser(@RequestBody final LoginDTO loginDTO) {
        return ResponseEntity.ok(userService.loginUser(loginDTO));
    }

    @GetMapping("/details")
    @ApiOperation(value = "User Details", notes = "Metoda umozliwiajaca pobranie informacji szczegolowych o uzytkowniku.")
    public ResponseEntity<String> getUserDetails() {
        return null;
    }

    @GetMapping("/update/token")
    @ApiOperation(value = "Refresh Token", notes = "Metoda umozliwiajaca odswiezenie tokenu dostepu")
    public ResponseEntity<String> refreshToken() {
        return null;
    }
}
