package com.karkon.ticket_booking.controller;

import com.karkon.ticket_booking.service.ScreeningService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/")
@Api(tags = "Screening Controller")
public class ScreeningController {

    private final ScreeningService screeningService;

    @GetMapping
    @ApiOperation(value = "Currently Playing", notes = "Metoda umozliwiajaca pobranie listy wyswietlanych w danym dniu filmow.")
    public ResponseEntity<List<String>> getCurrentlyPlaying() {
        LocalDate d = LocalDate.now();
        LocalTime t = LocalTime.MIN; //todo: przeniesc do serwisu
        LocalDateTime date = LocalDateTime.of(d, t);
        return ResponseEntity.ok(screeningService.getMoviesByScreeningDate(date));
    }
}
