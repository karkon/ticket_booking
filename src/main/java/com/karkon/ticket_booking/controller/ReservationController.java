package com.karkon.ticket_booking.controller;

import com.karkon.ticket_booking.dto.MakeReservationDTO;
import com.karkon.ticket_booking.dto.ReservationDTO;
import com.karkon.ticket_booking.model.Reservation;
import com.karkon.ticket_booking.service.ReservationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/reservations")
@Api(tags = "Reservation Controller")
public class ReservationController {
    private final ReservationService reservationService;

    @GetMapping("/all")
    @ApiOperation(value = "All Reservations", notes = "Metoda umozliwiajaca pobranie listy wszystkich rezerwacji uzytkownika.")
    public ResponseEntity<List<ReservationDTO>> getAllUserReservations(@RequestParam("user") String userId) {
        return ResponseEntity.ok(reservationService.getReservationsByUserId(userId));
    }

    @PostMapping("/new")
    @ApiOperation(value = "Make Reservation", notes = "Metoda umozliwiajaca utworzenie nowej rezerwacji.")
    public ResponseEntity<Reservation> makeReservation(@RequestBody final MakeReservationDTO makeReservationDTO) {
        return ResponseEntity.ok(reservationService.postReservation(makeReservationDTO));
    }

    @GetMapping("/details")
    @ApiOperation(value = "Reservation Details", notes = "Metoda umozliwiajaca pobranie informacji szczegolowych o rezerwacji.")
    public ResponseEntity<ReservationDTO> getReservationDetails(@RequestParam final Long reservationId) {
        return ResponseEntity.ok(reservationService.getReservationDetailsById(reservationId));
    }

}
