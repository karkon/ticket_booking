package com.karkon.ticket_booking.service;

import com.karkon.ticket_booking.repository.ScreeningRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ScreeningServiceImpl implements ScreeningService {

    private final ScreeningRepository screeningRepository;

    @Override
    public List<String> getMoviesByScreeningDate(LocalDateTime date) {
        return screeningRepository.getTitlesByDate(date);
    }

}
