package com.karkon.ticket_booking.service;

import com.karkon.ticket_booking.dto.GenreDTO;
import com.karkon.ticket_booking.dto.mapper.GenreDTOMapper;
import com.karkon.ticket_booking.model.Genre;
import com.karkon.ticket_booking.repository.GenreRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GenreServiceImpl implements GenreService {
    private final GenreRepository genreRepository;
    private final GenreDTOMapper genreDTOMapper;

    @Override
    public List<GenreDTO> getGenreDetailsByGenreId(String genreId) {
        List<Genre> genres = genreRepository.findByGenreId(genreId);
        return genres.stream().map(genreDTOMapper::fromGenreEntity).collect(Collectors.toList());
    }
}
