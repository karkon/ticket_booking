package com.karkon.ticket_booking.service;

import com.karkon.ticket_booking.dto.MakeReservationDTO;
import com.karkon.ticket_booking.dto.RealSeatDTO;
import com.karkon.ticket_booking.dto.ReservationDTO;
import com.karkon.ticket_booking.dto.SeatDTO;
import com.karkon.ticket_booking.model.Reservation;

import java.util.List;

public interface ReservationService {
    List<ReservationDTO> getReservationsByUserId(String userId);

    ReservationDTO getReservationDetailsById(Long reservationId);

    Reservation postReservation(MakeReservationDTO makeReservationDTO);


    SeatDTO getSeatByFeatures(int number, char row, int room);


    boolean isSeatProper(RealSeatDTO realSeatDTO);
}
