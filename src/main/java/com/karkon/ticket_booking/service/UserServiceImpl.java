package com.karkon.ticket_booking.service;

import com.karkon.ticket_booking.dto.LoginDTO;
import com.karkon.ticket_booking.dto.LoginResultDTO;
import com.karkon.ticket_booking.dto.UserDTO;
import com.karkon.ticket_booking.dto.mapper.UserDTOMapper;
import com.karkon.ticket_booking.model.User;
import com.karkon.ticket_booking.repository.UserRepository;
import com.karkon.ticket_booking.service.exception.CreateUserException;
import com.karkon.ticket_booking.service.exception.LoginUserException;
import com.karkon.ticket_booking.service.exception.NoRecordException;
import com.karkon.ticket_booking.service.exception.PatternException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.regex.Pattern;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    public static final String CREATE_USER_PATTERN = "^(.+)@(.+)$";
    public static final String CREATE_USER_STATUS_CREATED = "created";
    public static final String LOGIN_RESULT_STATUS_SUCCESS = "SUCCESS";
    public static final String LOGIN_RESULT_STATUS_ERROR = "ERROR";
    private final UserRepository userRepository;
    private final UserDTOMapper userDTOMapper;

    @Override
    public String createUser(final UserDTO userDTO) {
        if (!Pattern.matches(CREATE_USER_PATTERN, userDTO.getEmail())) {
            throw new PatternException(CREATE_USER_PATTERN + " " + userDTO.getEmail());
        }
        try {
            User user = userDTOMapper.fromUserDTO(userDTO);
            userRepository.save(user);
            return CREATE_USER_STATUS_CREATED;
        } catch (Exception e) {
            e.printStackTrace();
            throw new CreateUserException();
        }
    }

    @Override
    public LoginResultDTO loginUser(LoginDTO loginDTO) {

        LoginResultDTO loginResultDTO;
        Optional<User> candidate = userRepository.findUserByLoginPassword(loginDTO.getLogin(), loginDTO.getPassword());
        if (candidate.isPresent()) {
            UserDTO user = userDTOMapper.fromUserEntity(candidate.get());
            loginResultDTO = new LoginResultDTO(user.getToken(), LOGIN_RESULT_STATUS_SUCCESS);
        } else {
            throw new LoginUserException("login error");
        }
        return loginResultDTO;
    }

    @Override
    public UserDTO getUserByLogin(String login) {
        Optional<User> user = userRepository.findUserByLogin(login);
        if (user.isPresent()) {
            return userDTOMapper.fromUserEntity(user.get());
        } else {
            throw new NoRecordException("no user " + login);
        }
    }

}
