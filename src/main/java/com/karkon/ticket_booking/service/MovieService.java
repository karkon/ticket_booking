package com.karkon.ticket_booking.service;

import com.karkon.ticket_booking.dto.MovieDTO;

public interface MovieService {
    MovieDTO getDetailsById(Integer movieId);
}
