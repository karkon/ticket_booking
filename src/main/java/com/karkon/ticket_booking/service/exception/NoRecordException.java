package com.karkon.ticket_booking.service.exception;

public class NoRecordException extends RuntimeException {
    public NoRecordException() {
        super();
    }

    public NoRecordException(String message) {
        super("No record: " + message);
    }
}
