package com.karkon.ticket_booking.service.exception;

public class PatternException extends RuntimeException { //todo pattern syntax exception?

    public PatternException() {
        super();
    }

    public PatternException(String message) {
        super("Value not matching the pattern " + message);
    }
}
