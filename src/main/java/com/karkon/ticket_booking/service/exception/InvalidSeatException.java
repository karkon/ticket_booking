package com.karkon.ticket_booking.service.exception;

public class InvalidSeatException extends RuntimeException {

    public InvalidSeatException() {
        super();
    }

    public InvalidSeatException(String message) {
        super("Invalid seat " + message);
    }


}
