package com.karkon.ticket_booking.service.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class InvalidTermException extends RuntimeException {

    private int statusCode = 12345;

    public InvalidTermException() {
        super();
    }

    public InvalidTermException(String message) {
        super("Invalid Term " + message);
    }
}
