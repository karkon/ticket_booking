package com.karkon.ticket_booking.service.exception;

public class LoginUserException extends RuntimeException {

    public LoginUserException() {
        super();
    }

    public LoginUserException(String message) {
        super("Creating user failed");
    }
}
