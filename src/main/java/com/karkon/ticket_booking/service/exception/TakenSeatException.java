package com.karkon.ticket_booking.service.exception;

public class TakenSeatException extends RuntimeException {

    public TakenSeatException() {
        super();
    }

    public TakenSeatException(String message) {
        super("Taken seat " + message);
    }
}
