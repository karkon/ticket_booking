package com.karkon.ticket_booking.service.exception;

public class CreateUserException extends RuntimeException {

    public CreateUserException() {
        super();
    }

    public CreateUserException(String message) {
        super("Login user failed");
    }
}
