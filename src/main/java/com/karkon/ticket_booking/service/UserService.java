package com.karkon.ticket_booking.service;

import com.karkon.ticket_booking.dto.LoginDTO;
import com.karkon.ticket_booking.dto.LoginResultDTO;
import com.karkon.ticket_booking.dto.UserDTO;

public interface UserService {

    String createUser(UserDTO userDTO);

    LoginResultDTO loginUser(LoginDTO loginDTO);

    UserDTO getUserByLogin(String login);
}
