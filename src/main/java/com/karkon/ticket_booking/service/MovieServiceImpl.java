package com.karkon.ticket_booking.service;

import com.karkon.ticket_booking.dto.MovieDTO;
import com.karkon.ticket_booking.dto.mapper.MovieDTOMapper;
import com.karkon.ticket_booking.model.Movie;
import com.karkon.ticket_booking.repository.MovieRepository;
import com.karkon.ticket_booking.service.exception.NoRecordException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MovieServiceImpl implements MovieService {

    private final MovieRepository movieRepository;
    private final MovieDTOMapper movieDTOMapper;

    @Override
    public MovieDTO getDetailsById(Integer movieId) {
        Optional<Movie> movie = movieRepository.getDetails(movieId);
        if (movie.isPresent()) {
            return movieDTOMapper.fromMovieEntity(movie.get());
        } else {
            throw new NoRecordException("No movie with id " + movieId);
        }
    }
}
