package com.karkon.ticket_booking.service;

import com.karkon.ticket_booking.dto.GenreDTO;

import java.util.List;

public interface GenreService {
    List<GenreDTO> getGenreDetailsByGenreId(String genreId);
}
