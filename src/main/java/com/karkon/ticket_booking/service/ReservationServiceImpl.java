package com.karkon.ticket_booking.service;

import com.karkon.ticket_booking.dto.MakeReservationDTO;
import com.karkon.ticket_booking.dto.RealSeatDTO;
import com.karkon.ticket_booking.dto.ReservationDTO;
import com.karkon.ticket_booking.dto.SeatDTO;
import com.karkon.ticket_booking.dto.mapper.ReservationDTOMapper;
import com.karkon.ticket_booking.dto.mapper.SeatDTOMapper;
import com.karkon.ticket_booking.model.Reservation;
import com.karkon.ticket_booking.model.Seat;
import com.karkon.ticket_booking.model.StatusEnum;
import com.karkon.ticket_booking.repository.ReservationRepository;
import com.karkon.ticket_booking.repository.ScreeningRepository;
import com.karkon.ticket_booking.repository.SeatRepository;
import com.karkon.ticket_booking.repository.UserRepository;
import com.karkon.ticket_booking.service.exception.InvalidSeatException;
import com.karkon.ticket_booking.service.exception.InvalidTermException;
import com.karkon.ticket_booking.service.exception.NoRecordException;
import com.karkon.ticket_booking.service.exception.TakenSeatException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ReservationServiceImpl implements ReservationService {

    private final ReservationRepository reservationRepository;
    private final ReservationDTOMapper reservationDTOMapper;
    private final SeatRepository seatRepository;
    private final SeatDTOMapper seatDTOMapper;
    private final ScreeningRepository screeningRepository;
    private final UserRepository userRepository;

    @Override
    public List<ReservationDTO> getReservationsByUserId(String userId) {
        List<Reservation> reservations = reservationRepository.findByUserId(userId);
        return reservations.stream().map(reservationDTOMapper::fromReservationEntity).collect(Collectors.toList());
    }

    @Override
    public ReservationDTO getReservationDetailsById(Long reservationId) {
        Optional<Reservation> reservation = reservationRepository.getDetails(reservationId);
        if (reservation.isPresent()) {
            return reservationDTOMapper.fromReservationEntity(reservation.get());
        } else
            throw new NoRecordException("No reservation with id " + reservationId);
    }

    @Override
    public Reservation postReservation(MakeReservationDTO makeReservationDTO) {
        //todo: dodac sprawdzenie poprawnosci zajecia wybranego miejsca - czy po jego rezerwacji nie zostanie
        // jedno samotne

        checkTerm(makeReservationDTO.getRealSeatDTO().getTerm());
        checkSeatProper(makeReservationDTO.getRealSeatDTO());
        if (isSeatAvailable(makeReservationDTO.getRealSeatDTO()) && checkNeighbours(makeReservationDTO.getRealSeatDTO())) {

            Reservation reservation = prepareReservation(makeReservationDTO);

            // reservationRepository.save(reservation);

            return reservation;
        } else throw new TakenSeatException("Number " + makeReservationDTO.getRealSeatDTO().getNumber() +
                " row " + makeReservationDTO.getRealSeatDTO().getRow());

    }

    @Override
    public SeatDTO getSeatByFeatures(int number, char row, int room) {
        Optional<Seat> seat = seatRepository.findByFeatures(number, row, room);
        if (seat.isPresent()) {

            return seatDTOMapper.fromSeatEntity(seat.get());
        } else {
            throw new NoRecordException("No seat number " + number + " row " + row + " room " + room);
        }
    }


    public void checkSeatProper(RealSeatDTO realSeatDTO) {

        //czy w pokoju istnieje takie siedzenie
        Optional<Seat> realSeat = Optional.ofNullable(seatRepository.findByFeatures(realSeatDTO.getNumber(), realSeatDTO.getRow(),
                realSeatDTO.getRoom()).orElseThrow(() -> new InvalidSeatException("Seat not exists: number: "
                + realSeatDTO.getNumber() + " row " + realSeatDTO.getRow() + " room " + realSeatDTO.getRoom())));

    }

    private Reservation prepareReservation(MakeReservationDTO makeReservationDTO) {
        Reservation newReservation = new Reservation();

        newReservation.setStatus(StatusEnum.valueOf(makeReservationDTO.getStatus()));

        newReservation.setScreening(screeningRepository.findByFeatures(makeReservationDTO.getRealSeatDTO().getTerm(),
                makeReservationDTO.getRealSeatDTO().getRoom()).orElseThrow(() ->
                new NoRecordException("No screening term: " + makeReservationDTO.getRealSeatDTO().getTerm() + " room: "
                        + makeReservationDTO.getRealSeatDTO().getRoom())));

        newReservation.setSeat(seatRepository.findByFeatures(makeReservationDTO.getRealSeatDTO().getNumber(),
                makeReservationDTO.getRealSeatDTO().getRow(),
                makeReservationDTO.getRealSeatDTO().getRoom()).orElseThrow(() -> new NoRecordException("No seat " +
                makeReservationDTO.getRealSeatDTO().getNumber())));

        newReservation.setUser(userRepository.findById(makeReservationDTO.getUserLogin()).orElseThrow(() ->
                new NoRecordException("No user with login " + makeReservationDTO.getUserLogin())));

        return newReservation;
    }

    //todo: dodac metode sprawdzania czy miejsce jest wolne
    boolean isSeatAvailable(RealSeatDTO realSeatDTO) {
        return (checkRealSeatStatus(realSeatDTO) == null || StatusEnum.valueOf(checkRealSeatStatus(realSeatDTO)) == StatusEnum.CA);
    }

    String checkRealSeatStatus(RealSeatDTO realSeatDTO) {

        return reservationRepository.checkStatus(realSeatDTO.getNumber(), realSeatDTO.getRow(), realSeatDTO.getRoom(),
                realSeatDTO.getTerm(), realSeatDTO.getName());
    }

    //todo: dodac metode sprawdzania poprawnosci zajecia wybranego miejsca
    //metoda ktora bedzie pomocna w sprawdzaniu zajetosci miejsc sasiednich
    boolean isSeatExisting(int number, char row, int room) {
        Optional<Seat> seat = seatRepository.findByFeatures(number, row, room);
        return seat.isPresent();
    }

    boolean isSeatExisting(RealSeatDTO realSeatDTO) {
        Optional<Seat> seat = seatRepository.findByFeatures(realSeatDTO.getNumber(), realSeatDTO.getRow(),
                realSeatDTO.getRoom());
        return seat.isPresent();
    }

    boolean checkNeighbours(RealSeatDTO realSeatDTO) {
        RealSeatDTO neighbourMinusOne = new RealSeatDTO(realSeatDTO.getNumber() - 1, realSeatDTO.getRow(),
                realSeatDTO.getRoom(), realSeatDTO.getTerm(), realSeatDTO.getName());
        RealSeatDTO neighbourMinusTwo = new RealSeatDTO(realSeatDTO.getNumber() - 2, realSeatDTO.getRow(),
                realSeatDTO.getRoom(), realSeatDTO.getTerm(), realSeatDTO.getName());
        RealSeatDTO neighbourPlusOne = new RealSeatDTO(realSeatDTO.getNumber() + 1, realSeatDTO.getRow(),
                realSeatDTO.getRoom(), realSeatDTO.getTerm(), realSeatDTO.getName());
        RealSeatDTO neighbourPlusTwo = new RealSeatDTO(realSeatDTO.getNumber() + 2, realSeatDTO.getRow(),
                realSeatDTO.getRoom(), realSeatDTO.getTerm(), realSeatDTO.getName());
        if (!isSeatExisting(neighbourMinusOne) && !isSeatExisting(neighbourMinusTwo)) {
            return true;
        } else if (isSeatExisting(neighbourMinusOne) &&
                !isSeatAvailable(neighbourMinusOne)) {
            return true;
        } else if (isSeatExisting(neighbourMinusOne) && isSeatAvailable(neighbourMinusOne)
                && (isSeatExisting(neighbourMinusTwo) && isSeatAvailable(neighbourMinusTwo))) {
            return true;
        } else if (!isSeatExisting(neighbourPlusOne) && !isSeatExisting(neighbourPlusTwo)) {
            return true;
        } else if (isSeatExisting(neighbourPlusOne) && !isSeatExisting(neighbourPlusTwo) &&
                !isSeatAvailable(neighbourPlusOne)) {
            return true;
        } else if (isSeatExisting(neighbourPlusOne) && isSeatAvailable(neighbourPlusOne)
                && (isSeatExisting(neighbourPlusTwo) && isSeatAvailable(neighbourPlusTwo))) {
            return true;
        } else {
            throw new InvalidSeatException("wrong neighbours");
        }
    }


    //todo: dodac metode na sprawdzenie czy godzina skladania rezerwacji jest zgodna z oczekiwaniami

    void checkTerm(LocalDateTime term) {
        LocalDateTime actualDT = LocalDateTime.now();
        if (!actualDT.isBefore(term.minusMinutes(15))) {
            throw new InvalidTermException("actual " + actualDT + " screening term " + term);
        }
    }

    //todo do usuniecia ale przywrocone na potrzeby odpalenia testow
    @Override
    public boolean isSeatProper(RealSeatDTO realSeatDTO) {

        //czy w pokoju istnieje takie siedzenie
        Optional<Seat> realSeat = Optional.ofNullable(seatRepository.findByFeatures(realSeatDTO.getNumber(), realSeatDTO.getRow(),
                realSeatDTO.getRoom()).orElseThrow(() -> new InvalidSeatException("Seat: number: "
                + realSeatDTO.getNumber() + " row " + realSeatDTO.getRow() + " room " + realSeatDTO.getRoom())));

        //czy siedzenie jest wolne na ten seans

        //czy siedzenie jest poprawne (nie pozostanie otoczone jedno wolne miejsce)


        return realSeat.isPresent();
    }

}
