package com.karkon.ticket_booking.service;

import java.time.LocalDateTime;
import java.util.List;

public interface ScreeningService {

    List<String> getMoviesByScreeningDate(LocalDateTime date);
}
