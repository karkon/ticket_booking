package com.karkon.ticket_booking.model;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "screenings")
public class Screening {
    @Id
    @Column(name = "id_screening")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    private Integer id;
    @Column
    private LocalDateTime term; //todo: zmienic typ OffsetDateTime
    @JoinColumn(name = "id_movie")
    @ManyToOne//(fetch = FetchType.LAZY)
    private Movie movie;
    @JoinColumn(name = "id_room")
    @ManyToOne//(fetch = FetchType.LAZY)
    private Room room;
}
