package com.karkon.ticket_booking.model;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "reservations")
public class Reservation {
    @Id
    @Column(name = "id_reservation")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    private Long id;
    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private StatusEnum status;
    @JoinColumn(name = "login")
    @ManyToOne
    @NotNull
    private User user;
    @JoinColumn(name = "id_screening")
    @ManyToOne(cascade = CascadeType.ALL)
    @NotNull
    private Screening screening;
    @JoinColumn(name = "id_seat")
    @ManyToOne
    @NotNull
    private Seat seat;
}
