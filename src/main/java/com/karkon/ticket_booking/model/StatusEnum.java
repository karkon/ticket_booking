package com.karkon.ticket_booking.model;

public enum StatusEnum {
    CA("CANCELLED"),
    OK("BOOKED"),
    PE("PENDING");

    public final String label;

    StatusEnum(String label) {
        this.label = label;
    }
}
