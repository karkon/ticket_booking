package com.karkon.ticket_booking.model;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "movies")
public class Movie {
    @Id
    @Column(name = "id_movie")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    private Integer id;
    @Column
    @NotNull
    private String name;
    @Column
    @NotNull
    private Integer duration;
    @Column
    private String description;
    @Column
    private Integer minAgeLimit;
    @JoinColumn(name = "id_genre")
    @ManyToOne//(fetch = FetchType.LAZY)
    private Genre genre;
}
