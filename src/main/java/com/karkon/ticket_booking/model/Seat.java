package com.karkon.ticket_booking.model;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "seats")
public class Seat {
    @Id
    @Column(name = "id_seat")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    private Integer id;

    @Column
    private Integer number;

    @Column
    private Character row;

    @JoinColumn(name = "id_room", referencedColumnName = "id_room")
    @ManyToOne
    private Room room;
//    @EmbeddedId
//    SeatId id;
    //    @Id
//    @Column(name = "SEAT_NUMBER")
//    private int id;
//    @Id
//    @Column
//    private char row;

//    @JoinColumn(name = "id_room")
//    @ManyToOne
//    @MapsId("id")
//    private Room room;
}
