package com.karkon.ticket_booking.repository;

import com.karkon.ticket_booking.model.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GenreRepository extends JpaRepository<Genre, String> {

    @Query("SELECT genres FROM Genre genres")
    List<Genre> tescik();

    @Query("SELECT genre from Genre genre WHERE genre.id=?1")
    List<Genre> findByGenreId(String genreId);

}
