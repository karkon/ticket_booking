package com.karkon.ticket_booking.repository;

import com.karkon.ticket_booking.model.Screening;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface ScreeningRepository extends JpaRepository<Screening, Integer> {

    @Query("SELECT movie.name FROM Screening screening, Movie movie where screening.movie.id=movie.id AND screening.term > :today group by movie.name")
        //AND screening.term < :tomorrow
    List<String> getTitlesByDate(@Param("today") LocalDateTime date);

    @Query("SELECT screening FROM Screening screening where screening.term=?1 and screening.room.id=?2")
    Optional<Screening> findByFeatures(LocalDateTime term, int room);
}
