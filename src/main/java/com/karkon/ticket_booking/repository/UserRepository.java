package com.karkon.ticket_booking.repository;

import com.karkon.ticket_booking.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    @Query("SELECT user FROM User user WHERE user.login=?1 and user.password=?2")
    Optional<User> findUserByLoginPassword(String login, String Password);

    @Query("SELECT user FROM User user WHERE user.login=?1")
    Optional<User> findUserByLogin(String login);
}
