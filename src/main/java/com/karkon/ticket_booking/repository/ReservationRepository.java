package com.karkon.ticket_booking.repository;

import com.karkon.ticket_booking.model.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    @Query("SELECT reserv from Reservation reserv WHERE reserv.user.login=?1")
    List<Reservation> findByUserId(String userId);

    @Query("SELECT reserv from Reservation reserv WHERE reserv.id=?1")
    Optional<Reservation> getDetails(Long reservationId);

    @Query("SELECT reserv.status from Reservation reserv WHERE reserv.seat.number=?1 and reserv.seat.row=?2 and " +
            "reserv.seat.room.id=?3 and reserv.screening.term=?4 and reserv.screening.movie.name=?5")
    String checkStatus(int number, char row, int room, LocalDateTime term, String name);
    //todo zastanowic sie czy string name jest przydatne
}
