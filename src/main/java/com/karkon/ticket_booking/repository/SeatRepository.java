package com.karkon.ticket_booking.repository;

import com.karkon.ticket_booking.model.Seat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SeatRepository extends JpaRepository<Seat, Integer> {
    @Query("SELECT seat FROM Seat seat where seat.number=?1 and seat.row=?2 and seat.room.id=?3")
    Optional<Seat> findByFeatures(int number, char row, int room);
}
