package com.karkon.ticket_booking.repository;

import com.karkon.ticket_booking.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {

    @Query("SELECT movie from Movie movie WHERE movie.id=?1")
    Optional<Movie> getDetails(Integer movieId);
}
