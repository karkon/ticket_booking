package com.karkon.ticket_booking.dto;

import lombok.Data;

@Data
public class SeatDTO {
    private int id;
    private int number;
    private char row;
    private RoomDTO roomDTO;

    //private List<ReservationDTO> reservations;
}
