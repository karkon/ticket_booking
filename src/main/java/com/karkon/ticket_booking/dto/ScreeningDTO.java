package com.karkon.ticket_booking.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ScreeningDTO {
    private int id;
    private LocalDateTime term; //todo: zmienic typ
    private MovieDTO movieDTO;
    private RoomDTO roomDTO;
    //private List<ReservationDTO> reservations;
}
