package com.karkon.ticket_booking.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@AllArgsConstructor
@Data
public class RealSeatDTO {
    private int number;
    private char row;
    private int room;
    private LocalDateTime term;
    private String name;
}
