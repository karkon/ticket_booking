package com.karkon.ticket_booking.dto;

import lombok.Data;

@Data
public class RoomDTO {
    private Integer id;
    private String description;

    // private List<SeatDTO> seats;
    // private List<ScreeningDTO> screenings;
}
