package com.karkon.ticket_booking.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UserDTO {
    private String login;
    private String password;
    private String name;
    private String surname;
    private String email;
    private String token;
    private LocalDateTime tokenValidity;
    //private List<ReservationDTO> reservations;
}
