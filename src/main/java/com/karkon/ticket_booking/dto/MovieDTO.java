package com.karkon.ticket_booking.dto;

import lombok.Data;

@Data
public class MovieDTO {
    private int id;
    private String name;
    private int duration;
    private String description;
    private int minAgeLimit;
    private GenreDTO genreDTO;
}
