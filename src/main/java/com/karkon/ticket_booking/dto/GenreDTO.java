package com.karkon.ticket_booking.dto;

import lombok.Data;

@Data
public class GenreDTO {
    private String id;
    private String name;
    private String description;

  //  private List<MovieDTO> movies;
}
