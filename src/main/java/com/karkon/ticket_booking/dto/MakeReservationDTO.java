package com.karkon.ticket_booking.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class MakeReservationDTO {
    private String status;
    private RealSeatDTO realSeatDTO;
    private String userLogin;
}
