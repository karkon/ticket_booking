package com.karkon.ticket_booking.dto;

import lombok.Data;

@Data
public class ReservationDTO {
    private long id;
    private String status;
    private ScreeningDTO screeningDTO;
    private SeatDTO seatDTO;
    private UserDTO userDTO;
}
