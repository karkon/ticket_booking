package com.karkon.ticket_booking.dto.mapper;

import com.karkon.ticket_booking.dto.RoomDTO;
import com.karkon.ticket_booking.model.Room;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoomDTOMapper {
    RoomDTO fromRoomEntity(Room room);

    Room fromRoomDTO(RoomDTO roomDTO);
}
