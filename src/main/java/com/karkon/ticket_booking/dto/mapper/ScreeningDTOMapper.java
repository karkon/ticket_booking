package com.karkon.ticket_booking.dto.mapper;

import com.karkon.ticket_booking.dto.ScreeningDTO;
import com.karkon.ticket_booking.model.Screening;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {MovieDTOMapper.class, RoomDTOMapper.class})
public interface ScreeningDTOMapper {
    @Mappings({
            @Mapping(target = "movieDTO", source = "screening.movie"),
            @Mapping(target = "roomDTO", source = "screening.room")
    })
    ScreeningDTO fromScreeningEntity(Screening screening);

    @Mappings({
            @Mapping(target = "movie", source = "screeningDTO.movieDTO"),
            @Mapping(target = "room", source = "screeningDTO.roomDTO")
    })
    Screening fromScreeningDTO(ScreeningDTO screeningDTO);
}
