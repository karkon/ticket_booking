package com.karkon.ticket_booking.dto.mapper;

import com.karkon.ticket_booking.dto.ReservationDTO;
import com.karkon.ticket_booking.model.Reservation;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {ScreeningDTOMapper.class, SeatDTOMapper.class})
public interface ReservationDTOMapper {
    @Mappings({
            @Mapping(target = "screeningDTO", source = "reservation.screening"),
            @Mapping(target = "seatDTO", source = "reservation.seat"),
            @Mapping(target = "userDTO", source = "reservation.user")
    })
    ReservationDTO fromReservationEntity(Reservation reservation);

    @Mappings({
            @Mapping(target = "screening", source = "reservationDTO.screeningDTO"),
            @Mapping(target = "seat", source = "reservationDTO.seatDTO"),
            @Mapping(target = "user", source = "reservationDTO.userDTO")
    })
    Reservation fromReservationDTO(ReservationDTO reservationDTO);
}
