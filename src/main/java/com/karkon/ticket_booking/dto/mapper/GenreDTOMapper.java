package com.karkon.ticket_booking.dto.mapper;

import com.karkon.ticket_booking.dto.GenreDTO;
import com.karkon.ticket_booking.model.Genre;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface GenreDTOMapper {
    GenreDTO fromGenreEntity(Genre genre);

    Genre fromGenreDTO(GenreDTO genreDTO);
}
