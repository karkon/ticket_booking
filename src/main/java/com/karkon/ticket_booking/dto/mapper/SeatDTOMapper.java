package com.karkon.ticket_booking.dto.mapper;

import com.karkon.ticket_booking.dto.SeatDTO;
import com.karkon.ticket_booking.model.Seat;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = RoomDTOMapper.class)
public interface SeatDTOMapper {
    @Mapping(target = "roomDTO", source = "seat.room")
    SeatDTO fromSeatEntity(Seat seat);

    @Mapping(target = "room", source = "seatDTO.roomDTO")
    Seat fromSeatDTO(SeatDTO seatDTO);
}
