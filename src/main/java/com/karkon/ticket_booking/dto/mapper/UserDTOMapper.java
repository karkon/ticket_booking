package com.karkon.ticket_booking.dto.mapper;

import com.karkon.ticket_booking.dto.UserDTO;
import com.karkon.ticket_booking.model.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserDTOMapper {
    UserDTO fromUserEntity(User user);

    User fromUserDTO(UserDTO userDTO);
}
