package com.karkon.ticket_booking.dto.mapper;

import com.karkon.ticket_booking.dto.MovieDTO;
import com.karkon.ticket_booking.model.Movie;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = GenreDTOMapper.class)
public interface MovieDTOMapper {
    @Mapping(target = "genreDTO", source = "movie.genre")
    MovieDTO fromMovieEntity(Movie movie);

    @Mapping(target = "genre", source = "movieDTO.genreDTO")
    Movie fromMovieDTO(MovieDTO movieDTO);
}
