package com.karkon.ticket_booking.dto;

import lombok.Data;

@Data
public class LoginDTO {
    private String login;
    private String password;
}
