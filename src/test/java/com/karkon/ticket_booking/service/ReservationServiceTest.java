package com.karkon.ticket_booking.service;

import com.karkon.ticket_booking.dto.MakeReservationDTO;
import com.karkon.ticket_booking.dto.RealSeatDTO;
import com.karkon.ticket_booking.service.exception.InvalidSeatException;
import com.karkon.ticket_booking.service.exception.InvalidTermException;
import com.karkon.ticket_booking.service.exception.NoRecordException;
import com.karkon.ticket_booking.service.exception.TakenSeatException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@SpringBootTest
public class ReservationServiceTest {

    @Autowired
    private ReservationService SUT;

    @Test
    public void test_existingreservation_getReservationDetailsById_returns200() {
        var reservationDTO = SUT.getReservationDetailsById((long) 1);
        assertTrue("No reservation with this id", reservationDTO != null);
    }

    @Test
    public void test_correctdto_postReservation_returns200() {
        //z niewiadomych przyczyn test nie przechodzi jesli nie ustawi sie id rezerwacji (w swaggerze wszystko
        // przechodzi ok), aby wykorzystac ten test w praktyce, w metodzie post ustawic jakies id
        MakeReservationDTO makeReservationDTO = new MakeReservationDTO("PE", new RealSeatDTO(1, 'A',
                3, LocalDateTime.parse("2020-08-05T10:00:00"), "Turu. W pogoni za sławą")
                , "user1");
        var reservation = SUT.postReservation(makeReservationDTO);
        assertTrue("Post reservation error", reservation != null);
    }

    @Test
    public void test_badrecord_postReservation_throwNoRecordException() {
        MakeReservationDTO makeReservationDTO = new MakeReservationDTO("PE", new RealSeatDTO(0, 'A',
                0, LocalDateTime.parse("2020-08-05T10:00:00.000000"), "Turu. W pogoni za sławą")
                , "user1");
        Exception exception = assertThrows(NoRecordException.class, () -> SUT.postReservation(makeReservationDTO));

        String expectedMessage = "No record";
        String actualMessage = exception.getMessage();

        assertTrue("Wrong message", actualMessage.contains(expectedMessage));

    }

    @Test
    public void test_correctseat_isSeatExisting_returns200() {
        RealSeatDTO realSeatDTO = new RealSeatDTO(1, 'A', 3,
                LocalDateTime.parse("2020-08-05T10:00:00.000000"), "Turu. W pogoni za sławą");
        assertTrue("", SUT.isSeatProper(realSeatDTO));
    }

    @Test
    public void test_wrongseat_isSeatProper_throeInvalidSeatException() {
        RealSeatDTO realSeatDTO = new RealSeatDTO(0, 'B', 0,
                LocalDateTime.parse("2020-08-05T10:00:00.000000"), "Turu. W pogoni za sławą");
        Exception exception = assertThrows(InvalidSeatException.class, () -> SUT.isSeatProper(realSeatDTO));

        String expectedMessage = "Seat not exist";
        String actualMessage = exception.getMessage();

        assertTrue("Wrong message", actualMessage.contains(expectedMessage));
    }

    @Test
    public void test_badterm_postReservation_throwInvalidTermException() {
        MakeReservationDTO makeReservationDTO = new MakeReservationDTO("PE", new RealSeatDTO(1, 'A',
                3, LocalDateTime.parse("2020-08-05T10:00:00.000000"), "Turu. W pogoni za sławą")
                , "user1");
        Exception exception = assertThrows(InvalidTermException.class, () -> SUT.postReservation(makeReservationDTO));

        String expectedMessage = "Invalid Term";
        String actualMessage = exception.getMessage();

        assertTrue("Wrong message", actualMessage.contains(expectedMessage));
    }

    //    @Test
//    public void test_correctterm_postReservation_returns200() {
//        MakeReservationDTO makeReservationDTO = new MakeReservationDTO("PE",new RealSeatDTO(3, 'A', 1,
//                LocalDateTime.parse("2020-08-11T20:45:00"), "Coś się kończy, coś zaczyna"),"user1");
//        HttpStatus status = SUT.postReservation(makeReservationDTO).getStatusCode();
//        assertFalse("Kod bledu "+status, status.isError());
//        //test dziala tylko gdy w bazie nie ma faktycznie takiego rekordu, trzeba dodac mechanizm usuwania po wykonaniu
//        //testu ale to wymaga większych poprawek -> todo
//    }
    @Test
    public void test_correctterm_notavailable_postReservation_throwTakenSeatException() {
        MakeReservationDTO makeReservationDTO = new MakeReservationDTO("PE", new RealSeatDTO(3, 'A', 1,
                LocalDateTime.parse("2020-08-11T20:45:00"), "Coś się kończy, coś zaczyna"), "user1");
        Exception exception = assertThrows(TakenSeatException.class, () -> SUT.postReservation(makeReservationDTO));

        String expectedMessage = "Taken seat";
        String actualMessage = exception.getMessage();

        assertTrue("Wrong message", actualMessage.contains(expectedMessage));

        // zadbac o to zeby element istnial i byl dobry czasowo, mozna umiescic w tescie ale wymaga usuwania
    }

    @Test
    public void test_oneseatbreak_postReservation_throwInvalidSeatException() {
        MakeReservationDTO makeReservationDTO = new MakeReservationDTO("PE", new RealSeatDTO(2, 'A', 1,
                LocalDateTime.parse("2020-08-11T20:45:00"), "Coś się kończy, coś zaczyna"), "user1");
        Exception exception = assertThrows(InvalidSeatException.class, () -> SUT.postReservation(makeReservationDTO));

//        String expectedMessage = "Taken seat";
//        String actualMessage = exception.getMessage();
//
//        assertTrue("Wrong message", actualMessage.contains(expectedMessage));

    }
}
